# Bi18n - Internationalization module for Gothic 2 Online

```js

local messages = {
    en = {
        message = {
            hello = "Hello %s! Today is %d."
        }
    },
    pl = {
        message = {
            hello = "Witaj %s! Dzi� jest %d."
        }
    }
}

local i18n = Bi18n("en", messages)
Bi18n_setupGlobal(i18n)

// Event triggered when locale are changed
addEventHandler("Bi18n:onLocaleChanged", function (locale) {
    print("Locale changed to: " + locale)

    if (locale == "en") {
        setKeyLayout(0)
    } else if (locale == "pl") {
        setKeyLayout(1)
    }
})

local t = _t("message.hello", "Bimbol", 25)

print(t)
// Hello Bimbol! Today is 25.
print(_t("message.hello", "Bimbol", 25))
// Hello Bimbol! Today is 25.
print(_f("Custom formatter %s Yey!", t))  // Support for i18n-string
// Custom formatter Hello Bimbol! Today is 25. Yey!

i18n.setLocale("pl")

print(t)
// Witaj Bimbol! Dzi� jest 25.
print(_t("message.hello", "Bimbol", 25))
// Witaj Bimbol! Dzi� jest 25.
print(_f("Custom formatter %s Yey!", t))  // Support for i18n-string
// Custom formatter Witaj Bimbol! Dzi� jest 25. Yey!
```
