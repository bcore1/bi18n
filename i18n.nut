local global_i18n = null

///////////////////////////////////////////
/// Registry
///////////////////////////////////////////

// Global events
addEvent("Bi18n:onLocaleChanged")

//--------------------------------------------

class Bi18n {
    locale = null
    messages = null

    constructor(locale, translations) {
        this.locale = locale
        this.messages = {}

        foreach (locale, messages in translations) {
            this.messages[locale] <- this._buildHierarchy(messages)
        }
    }

    function _buildHierarchy(messages, root = null, hierarchy = null) {
        hierarchy = hierarchy || {}

        foreach (key, message in messages) {
            local path = root ? (root + "." + key) : key

            if (typeof(message) != "string") {
                this._buildHierarchy(message, path, hierarchy)
            } else {
                hierarchy[path] <- message
            }
        }

        return hierarchy
    }

    function setLocale(locale) {
        if (locale != this.locale) {
            this.locale = locale
            callEvent("Bi18n:onLocaleChanged", locale)
        }
    }

    function getLocale() {
        return this.locale
    }
    
    function translate(message, args = null) {
        return Bi18nString(this, message, args)
    }

    function formatter(message, args = null) {
        return Bi18nFormatter(message, args)
    }
}

//--------------------------------------------

class Bi18nString {
    parent = null
    message = null
    args = null

    constructor(parent, message, args) {
        this.parent = parent
        this.message = message
        this.args = args
    }

    function _typeof() {
        return "i18n-string"
    }

    function _tostring() {
        if (!(this.parent.locale in this.parent.messages)) {
            print("WARN! i18n: Locale " + this.parent.locale + " does not exist!")
            return this.message
        }

        local langugage = this.parent.messages[this.parent.locale]
        if (!(this.message in langugage)) {
            print("WARN! i18n: Cannot translate " + this.message + "!")
            return this.message
        }

        local message = langugage[this.message]
        local args = clone this.args

        if (args && args.len() > 0) {
            args.insert(0, this)
            args.insert(1, message)

            message = format.acall(args)
        }

        return message
    }
}

//--------------------------------------------

class Bi18nFormatter {
    message = null
    args = null

    constructor(message, args) {
        this.message = message
        this.args = args
    }

    function _typeof() {
        return "i18n-formatter"
    }

    function _tostring() {
        local message = this.message
        local args = clone this.args

        if (args && args.len() > 0) {
            args.insert(0, this)
            args.insert(1, typeof(message) != "string" ? message.tostring() : message)

            foreach (i, arg in args) {
                if (typeof(arg) == "i18n-string") {
                    args[i] = arg.tostring()
                }
            }

            message = format.acall(args)
        }

        return message
    }
}
//--------------------------------------------

function Bi18n_setupGlobal(bi18n) {
    global_i18n = bi18n
}

//--------------------------------------------

function _t(message, ...) {
    return global_i18n.translate(message, vargv)
}

//--------------------------------------------

function _f(message, ...) {
    return global_i18n.formatter(message, vargv)
}
